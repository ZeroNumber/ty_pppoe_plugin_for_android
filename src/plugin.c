#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <memory.h>
#include <stdio.h>

#include "md5.h"

#define MAXNAMELEN	256	/* max length of hostname or name for auth */
#define MAXSECRETLEN	256	/* max length of password or secret */

typedef unsigned char byte;

static byte saveuser[MAXNAMELEN] = {0};
static byte password[MAXSECRETLEN] = {0};

//算号函数（PIN为最终算得的账号）
static void getPIN(byte *username, byte *PIN)
{
	
	char key[100] =
		"aI0fC8RslXg6HXaKAUa6kpvcAXszvTcxYP8jmS9sBnVfIqTRdJS1eZNHmBjKN28j";
	char hex_time[200]; //十六进制时间戳
	unsigned i;

	//获取当前时间戳，并转换为十六进制
	time_t t;
	t = time(0);
	t = (long long)t;
	sprintf(hex_time, "%x", (int)t);

	//将用户名转换为大写
	for (i = 0; username[i] != '\0'; i++)
	{
		if (username[i] > 'a' && username[i] < 'z')
		{
			username[i] -= 32;
		}
	}

	//将密码ASCII值求和
	unsigned j, sum_pass = 0;
	for (j = 0; password[j] != '\0'; j++)
	{
		sum_pass += password[j];
	}

	//利用时间戳和密码长度进行处理
	int t_pass_len = 0;
	int t_flag = 0;
	int t_temp = 0;
	t_pass_len = ((int)t) % strlen(password);

	//计算t_flag
	if (t_pass_len < 1)
	{
		t_flag = 1;
	}
	else
	{
		t_flag = t_pass_len;
	}

	//根据时间戳和密码长度得到需要与密码ascii和相与的数
	int seed;
	if (t_flag == strlen(password))
	{
		seed = t_flag - 1;
	}
	else
	{
		seed = t_flag;
	}

	int a = (sum_pass ^ (seed)) & (0xffffffff);
	char password_hex[20];
	sprintf(password_hex, "%04x", a);

	//计算需要截取的第一部分密码长度
	unsigned pass_split_len = seed + 1;

	//截取第一部分密码
	char passwd_one[10];
	for (i = 0; i < pass_split_len; i++)
	{
		passwd_one[i] = password[i];
	}
	passwd_one[i] = '\0';

	char passwd_two[20];
	for (j = 0; password[i] != '\0'; i++)
	{
		passwd_two[j++] = password[i];
	}
	passwd_two[j] = '\0';

	//截取密钥第一部分，长度为60-len(passwd_one)
	char enc_key_one[60];
	for (i = 0; i < (60 - strlen(passwd_one)); i++)
	{
		enc_key_one[i] = key[i];
	}
	enc_key_one[i] = '\0';

	//截取密钥第二部分，长度为64-len(宽带用户名)-len(截取的密码的第二部分)
	char enc_key_two[60];
	for (j = 0; j < (64 - strlen(username) - strlen(passwd_two)); j++)
	{
		enc_key_two[j] = key[j];
	}
	enc_key_two[j] = '\0';

	// 组合待md5的字符串，
	//密钥的第一部分 + 截取的密码的第一部分 + 账号 + 密钥的第二部分 + 截取的密码的另一部分
	char key_str[200];
	int k, l, m, n, s;
	for (k = 0, i = 0; enc_key_one[i] != '\0'; k++)
	{
		key_str[k] = enc_key_one[i++];
	}
	for (l = k, i = 0; passwd_one[i] != '\0'; l++)
	{
		key_str[l] = passwd_one[i++];
	}
	for (m = l, i = 0; username[i] != '\0'; m++)
	{
		key_str[m] = username[i++];
	}
	for (n = m, i = 0; enc_key_two[i] != '\0'; n++)
	{
		key_str[n] = enc_key_two[i++];
	}
	for (s = n, i = 0; passwd_two[i] != '\0'; s++)
	{
		key_str[s] = passwd_two[i++];
	}
	key_str[s] = '\0';

	//将时间戳高低位互换
	long long swap_time_stamp;
	swap_time_stamp = (t << 24) & 0xFF000000;
	swap_time_stamp += (t << 8) & 0x00FF0000;
	swap_time_stamp += (t >> 8) & 0x0000FF00;
	swap_time_stamp += (t >> 24) & 0x000000FF;

	//取ASCII值
	int byte_four = (swap_time_stamp & 0xff000000) >> 24;
	int byte_three = (swap_time_stamp & 0x00ff0000) >> 16;
	int byte_two = (swap_time_stamp & 0x0000ff00) >> 8;
	int byte_one = swap_time_stamp & 0x000000ff;

	//组合一波待md5的字符串,组合方式：高低16位互换的时间戳+key_str
	int p = 0;
	unsigned char key_str_final[500]; //byte_one, byte_three 超过127，故应该用unsigned char类型
	key_str_final[0] = (char)byte_one;
	key_str_final[1] = (char)byte_two;
	key_str_final[2] = (char)byte_three;
	key_str_final[3] = (char)byte_four;

	for (p = 4, i = 0; key_str[i] != '\0'; p++)
	{
		key_str_final[p] = key_str[i++];
	}
	key_str_final[p] = '\0';

	//两次MD5
	unsigned char fir_md5[100];
	MD5_CTX md5;
	MD5Init(&md5);
	MD5Update(&md5, key_str_final, strlen((char *)key_str_final));
	MD5Final(&md5, fir_md5);
	fir_md5[16] = '\0';

	unsigned char sec_md5[100];
	MD5_CTX md6;
	MD5Init(&md6);
	MD5Update(&md6, fir_md5, strlen((char *)fir_md5));
	MD5Final(&md6, sec_md5);
	sec_md5[16] = '\0';

	unsigned char part_four[100];
	for (i = 0; i < (strlen(sec_md5) / 2); i++)
	{
		part_four[i] = sec_md5[i];
	}
	part_four[8] = '\0';

	char part_four_hex[100];
	char temp[10];
	for (i = 0, j = 0; part_four[i] != '\0'; i++)
	{
		sprintf(temp, "%02x", part_four[i]);
		part_four_hex[j] = temp[0];
		part_four_hex[j + 1] = temp[1];
		j += 2;
	}

	part_four_hex[j] = '\0';

	//收工，组装算得的账号

	for (i = 5, j = 0; hex_time[j] != '\0'; j++)
	{
		PIN[i++] = hex_time[j];
	}
	PIN[i++] = '2';
	PIN[i++] = '0';
	PIN[i++] = '2';
	PIN[i++] = '3';
	for (j = 0; part_four_hex[j] != '\0'; j++)
	{
		PIN[i++] = part_four_hex[j];
	}
	for (j = 0; password_hex[j] != '\0'; j++)
	{
		PIN[i++] = password_hex[j];
	}
	for (j = 0; username[j] != '\0'; j++)
	{
		PIN[i++] = username[j];
	}
	PIN[i] = '\0';
}

static int pap_modifyusername(byte *user, byte *passwd)
{
	byte PIN[MAXSECRETLEN] = {"~ghca"};

	getPIN(saveuser, PIN);

	strcpy(user, PIN);
}

// 生成PPPOE账号
void generate_pppoe_name(char user[256], char passwd[256])
{
	strcpy(saveuser, user);
	strcpy(password, passwd);
	// 计算账号
	pap_modifyusername(user, saveuser);
}

// 释放字符串所占内存
void free_string(char *str)
{
    // Free native memory in C which was allocated in C.
    free(str);
}
